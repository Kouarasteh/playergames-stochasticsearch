import random
import collections
from copy import deepcopy
import numpy as np
class GomokuMinimax:
    def __init__(self):
        self.board = []

    def printBoard(self):
        for x in range(7):
            for y in range(7):
                print('{0: <5}'.format(self.board[x][y]),end = ' ')
            print()
        print()




class State:
    def __init__(self,parent,color,board):
        self.board = board
        self.expUtility = 0
        self.children = []
        self.parent = parent
        self.color = color
        self.nextState = self.findNextState()

    def printBoard(self,state):
        b = self.genBoardfromList(state.board)
        for x in range(7):
            for y in range(7):
                print('{0: <5}'.format(b[x][y]),end = ' ')
            print()
        print()


    def getOpp(self,color):
        if(color == "BLUE"):
            return "RED"
        else:
            return "BLUE"

    def genBoardfromList(self,l):
        b = [[0,0,0,0,0,0,0],
             [0,0,0,0,0,0,0],
             [0,0,0,0,0,0,0],
             [0,0,0,0,0,0,0],
             [0,0,0,0,0,0,0],
             [0,0,0,0,0,0,0],
             [0,0,0,0,0,0,0]]
        for item in l:
            b[item[0][0]][item[0][1]] = item[1]
        return b



    def isTerminal(self):
        if self.parseWinningBlocks(self.board,self.color,5):
            return True
        else:
            return False

    def fillChildren(self):
        allWB = self.findWinningBlocks(self.color)
        nextStates = []
        for block in allWB:
            for loc in block:
                if loc not in self.board:
                    tempBoard = deepcopy(self.board)
                    tempBoard.append(((loc[0],loc[1]),self.color))
                    #print(self.color)
                    #print(loc)
                    child = State(self,self.getOpp(self.color),tempBoard)
                    child.board = tempBoard
                    #print(child.board)
                    #self.printBoard(child)
                    if child.isTerminal():
                        child.expUtility = 1000000
                    else:
                        child.expUtility = 0
                    self.children.append(child)

    def cascadeUtil(self):
        #print("cascade found")
        #print(self.color)
        if self.color == "RED":
            for child in self.children:
                child.fillChildren()
                for grandchild in child.children:
                    grandchild.fillChildren()
                    minmax = -1000000
                    for ggrandchild in grandchild.children:
                        temp = ggrandchild.findExpUtil()
                        if temp > minmax:
                            #print("minmax:",temp)
                            minmax = temp
                    grandchild.expUtility = minmax
                child.updateUtil()
            self.updateUtil()
        elif self.color == "BLUE":
            for child in self.children:
                child.fillChildren()
                for grandchild in child.children:
                    grandchild.fillChildren()
                    maxmin = 1000000
                    for ggrandchild in grandchild.children:
                        temp = ggrandchild.findExpUtil()
                        if temp < maxmin:
                            #print("maxmin:",temp)
                            maxmin = temp
                    grandchild.expUtility = maxmin
                child.updateUtil()
            self.updateUtil()

    def findNextState(self):
        nxt = self
        self.cascadeUtil()
        for child in self.children:
            print("my util:",self.expUtility)
            print("child util:",child.expUtility)
            if self.expUtility == child.expUtility:
                print("selected", child.board)
                nxt = child
        return nxt

    def updateUtil(self):
        if self.color == "RED":
            tempUtil = 0
            for childRed in self.children:
                if childRed.expUtility > tempUtil:
                    tempUtil = childRed.expUtility
        elif self.color == "BLUE":
            tempUtil = 1000000
            for childBlue in self.children:
                if childBlue.expUtility < tempUtil:
                    tempUtil = childBlue.expUtility

    def findExpUtil(self):
        if self.isTerminal() and self.color == "BLUE":
            return 1000000
        elif self.isTerminal() and self.color == "RED":
            return -1000000
        else:
            ret = self.findWinningBlocks(self.color)
            # print(ret)
            numRedFours = 0
            numRedThrees = 0
            numBlueFours = 0
            numBlueThrees = 0
            numBlueTwos = 0
            numRedTwos = 0
            for block in ret:
                temp = []
                # print(block)
                for item in block:
                    # print(item)
                    if ((item[0],item[1]),"RED") in self.board:
                        temp.append("RED")
                    if ((item[0],item[1]),"BLUE") in self.board:
                        temp.append("BLUE")
                # print(temp)
                if(temp.count("RED") == 4):
                    numRedFours+=1
                    #print("numRedFours: ", numRedFours)
                if(temp.count("BLUE") == 4):
                    numBlueFours+=1
                    #print("numBlueFours: ", numBlueFours)
                if(temp.count("RED") == 3):
                    numRedThrees+=1
                    #print("numRedThrees: ", numRedThrees)
                if(temp.count("BLUE") == 3):
                    numBlueThrees+=1
                    #print("numBlueThrees: ", numBlueThrees)
                if(temp.count("RED") == 2):
                    numRedTwos+=1
                    #print("numRedTwos: ", numRedTwos)
                if(temp.count("BLUE") == 2):
                    numBlueTwos+=1
                    #print("numBlueTwos: ", numBlueTwos)

            # print(block)
            # print((10*(numRedFours - numBlueFours) + 5*(numRedThrees - numBlueThrees) + (numRedTwos - numBlueTwos)))
            return (10*(numRedFours - numBlueFours) + 5*(numRedThrees - numBlueThrees) + (numRedTwos - numBlueTwos))




#all winning blocks of 4, then 3, then 2, then 1
    def maxWinningBlocks(self,color):
        ret = self.findWinningBlocks(color)
        res = []
        for block in ret:
            for item in block:
                if ((item[0],item[1]),"RED") in self.board:
                    temp.append("RED")
                if ((item[0],item[1]),"BLUE") in self.board:
                    temp.append("BLUE")
            if(temp.count(color) == 4):
                res.append(block)
        if not res:
            for block in ret:
                for item in block:
                    if ((item[0],item[1]),"RED") in self.board:
                        temp.append("RED")
                    if ((item[0],item[1]),"BLUE") in self.board:
                        temp.append("BLUE")
                if(temp.count(color) == 3):
                    res.append(block)
        if not res:
            for block in ret:
                for item in block:
                    if ((item[0],item[1]),"RED") in self.board:
                        temp.append("RED")
                    if ((item[0],item[1]),"BLUE") in self.board:
                        temp.append("BLUE")
                if(temp.count(color) == 2):
                    res.append(block)
        if not res:
            for block in ret:
                for item in block:
                    if ((item[0],item[1]),"RED") in self.board:
                        temp.append("RED")
                    if ((item[0],item[1]),"BLUE") in self.board:
                        temp.append("BLUE")
                if(temp.count(color) == 1):
                    res.append(block)
        return res

#finds consecutive runs within winning blocks
    def parseWinningBlocks(self,state,color,length):
        winnings = self.findWinningBlocks(color)
        res = []
        for block in winnings:
            # if (length == 3):
            #     temp = []
            #     temp = [self.board[block[0][0]][block[0][1]],self.board[block[1][0]][block[1][1]],self.board[block[2][0]][block[2][1]],self.board[block[3][0]][block[3][1]],self.board[block[4][0]][block[4][1]]]
            #     if temp == [0,color,color,color,0]:
            #         #print(temp)
            #         res.append(block)
            # elif (length == 4):
            #     temp = []
            #     temp = [state[block[0][0]][block[0][1]],state[block[1][0]][block[1][1]],state[block[2][0]][block[2][1]],state[block[3][0]][block[3][1]],state[block[4][0]][block[4][1]]]
            #     if ((temp == [0,color,color,color,color]) or (temp == [color,color,color,color,0])):
            #         #print(temp)
            #         res.append(block)
            #     #print(res)


            # if length == 5:
            #     temp = []
            #     temp = [state[block[0][0]][block[0][1]],state[block[1][0]][block[1][1]],state[block[2][0]][block[2][1]],state[block[3][0]][block[3][1]],state[block[4][0]][block[4][1]]]
            #     if temp == [color,color,color,color,color]:
            #         #print("temp: ",temp)
            #         res.append(block)
            temp = []
            for item in block:
                if ((item[0],item[1]),color) in self.board:
                    temp.append(color)
                if(temp.count(color) == 5):
                    res.append(block)
        return res
#returns all winning blocks
    def findWinningBlocks(self,color):
        allWinningBlocks = []
        newBlock = []
        #check for right-running winning blocks
        for x in range(7):
            for y in range(3):
                # if self.board[x][y] != self.getOpp(color):
                #     newBlock.append((x,y))
                if ((x,y),self.getOpp(color)) not in self.board :
                    newBlock.append((x,y))
                else:
                    continue
                for z in range(1,5):
                    if ((x,y+z),self.getOpp(color)) not in self.board:
                        newBlock.append((x,y+z))
                    else:
                        newBlock = []
                        break
                if len(newBlock) == 5:
                    allWinningBlocks.append(newBlock)
                newBlock = []

        #check for down-running winning blocks
        for x in range(3):
            for y in range(7):
                if ((x,y),self.getOpp(color)) not in self.board :
                    newBlock.append((x,y))
                else:
                    break
                for z in range(1,5):
                    if ((x+z,y),self.getOpp(color)) not in self.board:
                        newBlock.append((x+z,y))
                    else:
                        newBlock = []
                        break
                if len(newBlock) == 5:
                    allWinningBlocks.append(newBlock)
                newBlock = []

        #check for downright running winning blocks
        for x in range(3):
            for y in range(3):
                if ((x,y),self.getOpp(color)) not in self.board :
                    newBlock.append((x,y))
                else:
                    break
                for z in range(1,5):
                    if ((x+z,y+z),self.getOpp(color)) not in self.board:
                        newBlock.append((x+z,y+z))
                    else:
                        newBlock = []
                        break
                if len(newBlock) == 5:
                    allWinningBlocks.append(newBlock)
                newBlock = []

        #check for downleft running winning blocks
        for x in range(3):
            for y in range(4,7):
                if ((x,y),self.getOpp(color)) not in self.board :
                    newBlock.append((x,y))
                else:
                    break
                for z in range(1,5):
                    if ((x+z,y-z),color) in self.board or ((x+z,y-z),0) in self.board:
                        newBlock.append((x+z,y-z))
                    else:
                        newBlock = []
                        break
                if len(newBlock) == 5:
                    allWinningBlocks.append(newBlock)
                newBlock = []

        return allWinningBlocks

s = State(None,"RED",[])
s.board.append(((1,5),"RED"))
s.board.append(((5,1),"BLUE"))
currState = s
currState.board = s.board
s.printBoard(s)
while(not currState.isTerminal()):
    #print(currState.board)
    currState.fillChildren()
    print("filled")
    #print(currState.board)
    nxt = currState.findNextState()
    print("found")
    b = nxt.board
    currState = nxt
    currState.board = b
    currState.color = currState.getOpp(currState.color)
    print(b)
    #print(currState.board)
    currState.printBoard(currState)


currState.printBoard(currState)
