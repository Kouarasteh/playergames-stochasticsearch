class FactoryStepsASTAR:
    def __init__(self):
        self.distances = [[0   ,1064,673 ,1401,277 ],
                          [1064,0   ,958 ,1934,337 ],
                          [673 ,958 ,0   ,1001,399 ],
                          [1401,1934,1001,0   ,387 ],
                          [277 ,337 ,399 ,387 ,0   ]]
        self.w1 = [0,0,0,0,0]
        self.w2 = [0,0,0,0,0]
        self.w3 = [0,0,0,0,0]
        self.w4 = [0,0,0,0,0]
        self.w5 = [0,0,0,0,0]
        self.widgetOrders = [['A','E','D','C','A'],
                             ['B','E','A','C','D'],
                             ['B','A','B','C','E'],
                             ['D','A','D','B','D'],
                             ['B','E','C','B','D']]
        self.factSeq = []
        self.state = (self.factSeq,(self.w1,self.w2,self.w3,self.w4,self.w5))

    def printStates(self):
        print(self.state[0])
        print(self.state[1])

    def getLeastFilled(self):
        ret = 0
        min = 10
        for x in range(len(self.state[1])):
            if (sum(self.state[1][x]) <= min):
                ret = x
                min = sum(self.state[1][x])
        return ret

    def visitNewFactory(self,c):
        self.factSeq.append(c)
        for x in range(len(self.state[1])):
            for y in range(len(self.state[1][x])):
                if ((y == sum(self.state[1][x])) and (c == self.widgetOrders[x][y])):
                    self.state[1][x][y] = 1

    def isSolved(self):
        ret = True
        for x in self.state[1]:
            if sum(x) != 5:
                ret = False
        return ret

    def ASTAR(self):
        while(self.isSolved() is not True):
            mostNeedyWidgetIDX = self.getLeastFilled()
            #print(mostNeedyWidgetIDX)
            #print(self.state[1][mostNeedyWidgetIDX])
            #print(self.state[1][mostNeedyWidgetIDX][sum(self.state[1][mostNeedyWidgetIDX])])
            nextFactory = self.widgetOrders[mostNeedyWidgetIDX][sum(self.state[1][mostNeedyWidgetIDX])]
            print(nextFactory)
            self.visitNewFactory(nextFactory)

f = FactoryStepsASTAR()
print(f.distances[0][1])
print(f.state[1][0][0])
f.printStates()
print(f.isSolved())
f.ASTAR()
f.printStates()
