#Gomoku Agents
The Gomoku reflex agent follows an intuitive set of instructions to respond to a given game scenario to ensure its victory and prevent the victory of the opponent. However, it is suboptimal.

The Gomoku Minimax agent develops a 3-deep Game Decision tree using Minimax format, and selects the move that maximizes its own return 3 steps ahead.

#Factory Problems
This is a stochastic example of  path planning through minimizing edges traveled on a map of factory locations to create certain widgets that require several parts from several factories.