import random
import collections

class GomokuReflex:
    def __init__(self):
        self.board = [[0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0]]

    def printBoard(self):
        for x in range(7):
            for y in range(7):
                print('{0: <5}'.format(self.board[x][y]),end = ' ')
            print()
        print()

    def firstMove(self,color):
        xPos = random.randint(0,6)
        yPos = random.randint(0,6)
        while(self.board[xPos][yPos] != 0):
            xPos = random.randint(0,6)
            yPos = random.randint(0,6)
        self.placeStone((xPos,yPos),color)

    def redMove(self):
        return False

    def blueMove(self):
        return False

    def placeStone(self, xyPos, color):
        if self.board[xyPos[0]][xyPos[1]] != 0:
            print("ILLEGAL PLACEMENT")
        self.board[xyPos[0]][xyPos[1]] = color

    def boardFull(self):
        for x in range(7):
            for y in range(7):
                if(self.board[x][y] == 0):
                    return False
        print("BOARD FULL")
        return True

    def getOpp(self,color):
        if(color == "BLUE"):
            return "RED"
        else:
            return "BLUE"

    def getBestNextMove(self,color):
        opp = self.getOpp(color)
        #option 1
        ret = self.oneAway(color)
        #print(ret)
        if ret != "NO":
            print("ONEAWAY")
            return ret
        #option 2
        fours = self.parseWinningBlocks(opp,4)
        if fours:
            for block in fours:
                for idx in block:
                    if self.board[idx[0]][idx[1]] == 0:
                        return idx
        #option 3
        threes = self.parseWinningBlocks(opp,3)
        ret = []
        if threes:
            for block in threes:
                for idx in block:
                    if self.board[idx[0]][idx[1]] == 0:
                        ret.append(idx)

            if len(ret) == 1:
                return ret[0]
            else:
                #same y, vertical run
                if ret[0][1] == ret[1][1]:
                    if ret[0][0] > ret[1][0]:
                        return ret[0]
                    else:
                        return ret[1]
                #same x, horizontal run
                elif ret[0][0] == ret[1][0]:
                    if ret[0][1] < ret[1][1]:
                        return ret[0]
                    else:
                        return ret[1]
                #downright, where ret[1] is returned
                elif ret[0][0] > ret[1][0] and ret[0][1] < ret[1][1]:
                    return ret[1]
                #downleft where ret[1] is returned
                elif ret[0][0] < ret[1][0] and ret[0][1] > ret[1][1]:
                    return ret[1]
                else:
                    return ret[0]
        #option 4
        maxes = self.maxWinningBlocks(color)
        options = []
        for block in maxes:
            for idx in block:
                if self.board[idx[0]][idx[1]] == 0:
                    options.append(idx)
        mini = 10
        result = []
        finalres = (-1,-1)
        for option in options:
            if option[1] < mini:
                mini = option[1]
                result = []
                result.append(option)
            elif option[1] == mini:
                result.append(option)
        maxi = 0
        if len(result) > 1:
            for option in result:
                if option[0] > maxi:
                    maxi = option[0]
                    finalres = option
            return finalres
        else:
            if result:
                finalres = result[0]
                return finalres


#all winning blocks of 4, then 3, then 2, then 1
    def maxWinningBlocks(self,color):
        ret = self.findWinningBlocks(color)
        res = []
        for block in ret:
            temp = [self.board[block[0][0]][block[0][1]],self.board[block[1][0]][block[1][1]],self.board[block[2][0]][block[2][1]],self.board[block[3][0]][block[3][1]],self.board[block[4][0]][block[4][1]]]
            if(temp.count(color) == 4):
                res.append(block)
        if not res:
            for block in ret:
                temp = [self.board[block[0][0]][block[0][1]],self.board[block[1][0]][block[1][1]],self.board[block[2][0]][block[2][1]],self.board[block[3][0]][block[3][1]],self.board[block[4][0]][block[4][1]]]
                if(temp.count(color) == 3):
                    res.append(block)
        if not res:
            for block in ret:
                temp = [self.board[block[0][0]][block[0][1]],self.board[block[1][0]][block[1][1]],self.board[block[2][0]][block[2][1]],self.board[block[3][0]][block[3][1]],self.board[block[4][0]][block[4][1]]]
                if(temp.count(color) == 2):
                    res.append(block)
        if not res:
            for block in ret:
                temp = [self.board[block[0][0]][block[0][1]],self.board[block[1][0]][block[1][1]],self.board[block[2][0]][block[2][1]],self.board[block[3][0]][block[3][1]],self.board[block[4][0]][block[4][1]]]
                if(temp.count(color) == 1):
                    res.append(block)
        return res
#finds consecutive runs within winning blocks
    def parseWinningBlocks(self,color,length):
        winnings = self.findWinningBlocks(color)
        res = []
        for block in winnings:
            if (length == 3):
                temp = []
                temp = [self.board[block[0][0]][block[0][1]],self.board[block[1][0]][block[1][1]],self.board[block[2][0]][block[2][1]],self.board[block[3][0]][block[3][1]],self.board[block[4][0]][block[4][1]]]
                if temp == [0,color,color,color,0]:
                    #print(temp)
                    res.append(block)
            elif (length == 4):
                temp = []
                temp = [self.board[block[0][0]][block[0][1]],self.board[block[1][0]][block[1][1]],self.board[block[2][0]][block[2][1]],self.board[block[3][0]][block[3][1]],self.board[block[4][0]][block[4][1]]]
                if ((temp == [0,color,color,color,color]) or (temp == [color,color,color,color,0])):
                    #print(temp)
                    res.append(block)
                #print(res)
            elif length == 5:
                temp = []
                temp = [self.board[block[0][0]][block[0][1]],self.board[block[1][0]][block[1][1]],self.board[block[2][0]][block[2][1]],self.board[block[3][0]][block[3][1]],self.board[block[4][0]][block[4][1]]]
                if temp == [color,color,color,color,color]:
                    #print("temp: ",temp)
                    res.append(block)
        return res

#returns all winning blocks
    def findWinningBlocks(self,color):
        allWinningBlocks = []
        newBlock = []
        #check for right-running winning blocks
        for x in range(7):
            for y in range(3):
                if self.board[x][y] != self.getOpp(color):
                    newBlock.append((x,y))
                else:
                    continue
                for z in range(1,5):
                    if self.board[x][y+z] != self.getOpp(color):
                        newBlock.append((x,y+z))
                    else:
                        newBlock = []
                        break
                if len(newBlock) == 5:
                    allWinningBlocks.append(newBlock)
                newBlock = []

        #check for down-running winning blocks
        for x in range(3):
            for y in range(7):
                if self.board[x][y] != self.getOpp(color):
                    newBlock.append((x,y))
                else:
                    break
                for z in range(1,5):
                    if self.board[x+z][y] != self.getOpp(color):
                        newBlock.append((x+z,y))
                    else:
                        newBlock = []
                        break
                if len(newBlock) == 5:
                    allWinningBlocks.append(newBlock)
                newBlock = []

        #check for downright running winning blocks
        for x in range(3):
            for y in range(3):
                if self.board[x][y] != self.getOpp(color):
                    newBlock.append((x,y))
                else:
                    break
                for z in range(1,5):
                    if self.board[x+z][y+z] != self.getOpp(color):
                        newBlock.append((x+z,y+z))
                    else:
                        newBlock = []
                        break
                if len(newBlock) == 5:
                    allWinningBlocks.append(newBlock)
                newBlock = []

        #check for downleft running winning blocks
        for x in range(3):
            for y in range(4,7):
                if self.board[x][y] != self.getOpp(color):
                    newBlock.append((x,y))
                else:
                    break
                for z in range(1,5):
                    if self.board[x+z][y-z] == color or self.board[x+z][y-z] == 0:
                        newBlock.append((x+z,y-z))
                    else:
                        newBlock = []
                        break
                if len(newBlock) == 5:
                    allWinningBlocks.append(newBlock)
                newBlock = []

        return allWinningBlocks

    def oneAway(self,color):
        for x in range(7):
            for y in range(7):
                if((self.board[x][y] != "RED") and (self.board[x][y] != "BLUE")):
                    self.placeStone((x,y),color)
                    #print(self.parseWinningBlocks(color,5))
                    if self.parseWinningBlocks(color,5):
                        print("ONEAWAY")
                        self.board[x][y] = 0
                        return (x,y)
                    self.board[x][y] = 0
        return "NO"

g = GomokuReflex()
g.printBoard()
g.placeStone((5,1),"RED")
g.placeStone((1,5),"BLUE")
g.printBoard()
while((not g.parseWinningBlocks("RED",5)) and (not g.parseWinningBlocks("BLUE",5))):
        nxt = g.getBestNextMove("RED")
        print("RED: ",nxt)
        g.placeStone(nxt,"RED")
        if((not g.parseWinningBlocks("RED",5)) and (not g.parseWinningBlocks("BLUE",5))):
                nxt = g.getBestNextMove("BLUE")
                print("BLUE: ",nxt)
                g.placeStone(nxt,"BLUE")
g.printBoard()
